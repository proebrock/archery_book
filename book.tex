\documentclass[12pt,oneside]{article}

\usepackage{geometry}
\geometry{ % A4 size is 210mm x 297mm
a4paper,
total={170mm,257mm},
left=20mm,
top=20mm,
}

\usepackage[T1]{fontenc}
\usepackage{ngerman}

\hyphenation{Wurf-arm Cen-ter-shot}

\newcommand{\Q}[1]{\glqq #1\grqq}

\usepackage{units}
\usepackage{xfrac}
\usepackage{amssymb}

\usepackage[skip=1pt plus1pt, indent=0pt]{parskip}

\setcounter{tocdepth}{2}
\setcounter{secnumdepth}{2}

%
% Figures
%

\usepackage{graphicx}
\usepackage{wrapfig}

\renewcommand{\figurename}{Abb.}
\newcommand{\figref}[1]{Abb. \ref{fig:#1}}

% Parameters: filename, position, scale, label, caption
\newcommand{\floatfig}[5]{%
	\begin{figure}[#2]%
	\centering%
	\includegraphics[width=#3\textwidth]{#1}%
	\caption{#5}%
	\label{fig:#4}%
	\end{figure}}

% Parameters: filename, position, scale, label, caption
\newcommand{\wrapfig}[5]{%
	\begin{wrapfigure}{#2}{#3\textwidth}%
	\includegraphics[width=#3\textwidth]{#1}%
	\caption{#5}%
	\label{fig:#4}%
	\end{wrapfigure}}

%
% Tables
%

\renewcommand{\tablename}{Tab.}
\newcommand{\tabref}[1]{Tab. \ref{tab:#1}}

% Parameters: position, columns
\newcommand{\floattabstart}[2]{%
	\begin{table}[#1]%
	\centering%
	\begin{tabular}{#2}}

% Parameters: label, caption
\newcommand{\floattabend}[2]{%
	\end{tabular}%
	\caption{#2}%
	\label{tab:#1}%
	\end{table}}

%
% Title page
%

\title{Technik-Handbuch\\[1ex]für Recurve-Bögen}
\author{Bogenschützen-Club Chur}
\date{\today}

% Re-define title environment
\makeatletter
\def\@maketitle{
\raggedright
\begin{center}
{\Huge \bfseries \sffamily \@title}\\[4ex]
{\Large \@author}\\[4ex]
\@date\\[8ex]
\includegraphics[width=40mm]{images/club-logo.png}
\end{center}}
\makeatother



\begin{document}
\maketitle
\newpage
\tableofcontents

\section*{Danksagung}

Vielen Dank an alle, die bei der Erstellung des Handbuchs geholfen haben.\\

Danke an GregorDS an die Bereitstellung der Grafikvorlage für den Recurve-Bogen unter CC Lizenz.

\newpage

\section{Bogen}

\subsection{Bogenlänge}

Die {\em Bogenlänge} (englisch {\em bow length}) gibt die Gesamtlänge des Bogens an. Diese wird in Zoll gemessen. Bei korrekter Standhöhe entspricht die Bogenlänge der Sehnenlänge plus 3\unit{''}.\\

Für zerlegbare Bögen ist die Standard-Länge des Mittelstücks 25\unit{''}. Zusammen mit der Angabe auf den Wurfarmen, z.B. 68\unit{''} ergibt sich eine Bogenlänge von 68\unit{''}. Montiert man dieselben Wurfarme auf ein Mittelstück der Länge 19\unit{''}, also 6\unit{''} kürzer als die Standard-Länge, erhält man einen Bogen der Länge 62\unit{''}.\\

Die Bogenlänge sollte grob zur Auszugslänge der Schützin passen. Siehe \tabref{bow_height} für ein paar Richtwerte.\\

\floattabstart{htb}{cc}
Auszugslänge (AMO) & Bogenlänge\\
\hline
27\unit{''} & 66\unit{''}\\
28\unit{''} & 68\unit{''}\\
29\unit{''} & 70\unit{''}\\
30\unit{''} & 70\unit{''}\\
31\unit{''} & 72\unit{''}\\
\hline
\floattabend{bow_height}{Bogenlänge}

Ein Bogen mit grösserer Länge lässt sich allgemein stabiler halten (Trägheit). Aber je nach Anwendung kann ein langer Bogen unhandlich sein (3D oder Jagd).

\subsection{Standhöhe}

\subsubsection{Messen}

\wrapfig{drawings/brace_height.pdf}{l}{0.2}{brace_height}{Standhöhe}

Die {\em Standhöhe} (englisch {\em brace height}) ist der senkrechte Abstand von der Bogensehne zum tiefsten Punkt des Griffstücks (siehe \figref{brace_height}) und wird in Zoll oder Zentimetern angegeben.\\

Am einfachsten kann man die Standhöhe mit dem Sehnenmassstab messen. Beim Messen auf einen rechten Winkel zur Sehne achten!

\subsubsection{Einstellen}

Die Standhöhe hängt direkt mit der Länge der Sehne zusammen. Je kürzer die Sehne, desto grösser die Standhöhe. Ist die Standhöhe zu klein, kann die Sehne durch weiteres Eindrehen verkürzt werden (auf Drehrichtung achten!). Dadurch wird die Standhöhe grösser. Ist die Standhöhe zu gross kann man versuchen die Sehne auszudrehen. Hat die Sehne aber kaum noch Windungen, und die Standhöhe ist immer noch zu gross, muss eine neue Sehne her.\\

Über die Zeit dehnt sich die Sehne je nach Material etwas. Und bei der Lagerung oder beim Auf- und Abziehen der Sehne dreht sich diese etwas aus. Meist führt dies zu einer zu kleinen Standhöhe, welche man durch Nachmessen erkennt und durch Eindrehen korrigiert.

\subsubsection{Sollwert}

Manchmal kommen Bögen und Wurfarme mit einem Infoblatt oder gar Handbuch des Herstellers. Dann gelten natürlich dessen Angaben. Falls nicht, gibt \tabref{brace_height} grobe Richtwerte für die richtige Standhöhe.

\floattabstart{htb}{cc}
Bogenlänge & Standhöhe\\
\hline
64\unit{''} & 21.0 - 21.6\unit{cm}\\
66\unit{''} & 21.3 - 21.9\unit{cm}\\
68\unit{''} & 21.6 - 22.2\unit{cm}\\
70\unit{''} & 21.7 - 22.5\unit{cm}\\
\hline
\floattabend{brace_height}{Standhöhe}

\subsubsection{Auswirkungen}

Eine grössere Standhöhe führt zu einem kleineren Weg auf dem der Pfeil beschleunigt wird, der Pfeil fliegt also langsamer. Dafür schiesst der Bogen ruhiger und vibrationsärmer und das System ist fehlerverzeihender.



\subsection{Nockpunkthöhe}

\subsubsection{Messen}

\wrapfig{drawings/nocking_point_height.pdf}{l}{0.2}{nocking_point_height}{Nockpunkthöhe}

Zur Messung der {\em Nockpunkthöhe} (englisch {\em nocking point height}) wird der Sehnenmassstab bei aufrechtem Bogen auf die Sehne aufgesetzt und so an der Sehne verschoben, dass der lange Arm auf der Pfeilauflage zu liegen kommt und diese leicht berührt (siehe \figref{nocking_point_height}).\\

Am Sehnenmassstab kann nun die Nockpunkthöhe abgelesen werden. Nullpunkt ist die Höhe, mit der der Sehnenmassstab auf der Pfeilauflage liegt. Der Nockpunkt sollte darüber liegen. Die Nockpunkthöhe wird bis zur Unterseite des Nockpunktes gemessen (siehe \figref{nocking_point_height_closeup}).

\subsubsection{Einstellen}

Das Einstellen der Nockpunkthöhe hängt vom Typ des Nockpunktes ab. Messing-Nockpunkte lassen sich mit einer Nockpunktzange öffnen, verschieben und wieder schliessen.\\

Selbst gebundene Nockpunkte lassen sich manchmal verschieben oder müssen entfernt und neu gebunden werden. Gerade solange man noch mit Tuning beschäftigt ist, sollte man den Nockpunkt verschieben können. Danach kann man ihn mit Sekundenkleber oder besser mit Bienenwachs fixieren.

\subsubsection{Sollwert}

\wrapfig{drawings/nocking_point_height_closeup.pdf}{r}{0.2}{nocking_point_height_closeup}{Nockpunkthöhe (Detail)}

Die Nockpunkthöhe sollte bei etwa 0.5\unit{''} oder etwa 12\unit{mm} liegen.\\

Will man die perfekte Nockpunkthöhe herausfinden, kann man diese mittels {\em Bareshaft-Tuning} herausfinden: Man schiesst befiederte und unbefiederte (\Q{bare}) Pfeile immer abwechselnd auf eine bestimmt Distanz, z.B. 18\unit{m}. Im Idealfall landen beide Arten Pfeile auf derselben Höhe. Gruppieren sich die unbefiederten Pfeile oben, ist der Nockpunkt zu niedrig, gruppieren sich die unbefiederten Pfeile eher unten, ist der Nockpunkt zu hoch. Diese Art von Tuning erfordert, dass die Schützin Pfeile sauber gruppieren kann, für Einsteigerinnen ist dieses Tuning daher nicht zu empfehlen.

\subsubsection{Auswirkungen}

Ein falsch positionierter Nockpunkt führt zu suboptimalem Pfeilflug, der Pfeil \Q{reitet} (englisch {\em porpoising}). Im Fall eines sehr niedrigen Nockpunktes kann der Pfeil beim Beschleunigen mit dem Bogen kollidieren.



\subsection{Tiller}

\subsubsection{Messen}

\wrapfig{drawings/tiller.pdf}{l}{0.2}{tiller}{Tiller}

Zum Bestimmen des {\em Tillers} misst man - ähnlich bei Messung der Standhöhe senkrecht von der Sehne zum Bogen, allerdings an den zwei Stellen, wo die Wurfarme jeweils auf das Mittelstück treffen (siehe \figref{tiller}). Dann zieht man vom oberen Abstand den unteren Abstand ab und erhält den Tiller.\\

Für gewöhnlich ist der obere Abstand grösser als der untere Abstand. Dann ist der Tiller positiv. Der Tiller kann aber auch negativ sein, wenn der obere Abstand kleiner ist als der untere Abstand.

\subsubsection{Einstellen}

Einstellen lässt sich der Tiller meist nur bei technischen Recurve-Bögen, zum Beispiel mit ILF-System zur Befestigung der Wurfarme. Dazu hat es zwei Schrauben, welche den Winkel der Wurfarme zum Mittelstück festlegen. Oft sind diese Schrauben noch mit einer zweiten Schraube von innen gegen-gesichert, die man zuerst öffnen muss. Ausserdem sollte man diese Einstellung bei entspanntem Bogen machen, sonst zerkratzt die Tillerschraube den Wurfarm.\\

Um den Tiller zu erhöhen, schraubt man die obere Tiller-Schraube dieselbe Anzahl Umdrehungen raus (weniger Spannung) wie man die untere Tillerschraube rein dreht (mehr Spannung). Meist reichen je halbe Drehungen aus, bevor man wieder nachmisst.\\

Achtung: Wann immer wenn man an den Tillerschrauben dreht, muss man sicherstellen, dass man keine Schraube zu weit raus dreht, dass sie nicht mehr genug Halt hat. Sonst erlebt man unter Umständen eine böse Überraschung beim Ausziehen des Bogens.

\subsubsection{Sollwert}

Schiesst man mit mediterranem Griff, ist ein Tiller von +6\unit{mm} sinnvoll. Schiesst man mit drei Fingern unter dem Pfeil, wird ein Tiller von 0\unit{mm} empfohlen. Für Barebow-Schützinnen, welche String-Walking machen, macht ein leicht negativer Tiller Sinn.

\subsubsection{Auswirkungen}

Grundsätzlich bestimmt der Tiller wie sich bei ausgezogenem Bogen die Kraft auf die Wurfarme verteilt. Bei positivem Tiller wird der untere Wurfarm stärker belastet als bei negativem Tiller.\\

Je nach Stil den man schiesst und wo man die Sehne greift, belastet man die Wurfarme unterschiedlich. Mit mediterranem Griff belastet man den oberen Wurfarm stärker, weil man die Sehne weiter oben greift. Mit der entsprechenden Verstellung des Tillers kann man dies kompensieren.\\

Zum Beispiel: Greift man beim String-Walking die Sehne eher tief, belastet man den unteren Wurfarm mehr. Dies kann man mit einem negativen Tiller kompensieren, welcher die Belastung mehr auf den oberen Wurfarm verteilt, so dass die Belastung für die Wurfarme unter dem Strich wieder gleich ist.\\

Bei ungleich belasteten Wurfarmen ist das Timing beim Zurückschnellen nach der Schussabgabe ungünstig. Der schwächer belastete Wurfarm erreicht zuerst die Nullposition und vibriert, während der stärker belastete Wurfarm noch beschleunigt. Ergebnis ist ein lauteres Vibrieren und unter Umständen eine ungünstige Beeinflussung des Pfeils nach oben oder unten. Beim Zielen kann der Bogen ausserdem die Tendenz haben, nach oben oder nach unten zu ziehen.



\subsection{Auszugslänge}

\wrapfig{drawings/draw_length.pdf}{r}{0.2}{draw_length}{Auszugslänge}

Die {\em Auszugslänge} (englisch {\em draw length}) beschreibt im Grunde, wie weit die Schützin den Bogen auszieht, sie ist also eher eine Eigenschaft der Schützin als des Bogens. Die genaue Definition der sogenannten {\em AMO draw length} ist der Abstand vom tiefsten Punkt der Nocke ({\em Nockboden}, also da wo die Sehne ist) bis zum zum tiefsten Punkt des Griffstücks plus zusätzliche $1\,\sfrac{3}{4}\,\unit{''}$.\\

Bei technischen Recurve-Bögen liegt der tiefsten Punkt des Griffstücks oft auf Höhe des Buttons. Dann legt die Schützin einen Pfeil ein und eine seitlich stehende Helferin (Achtung Sicherheit!) markiert nach dem Ausziehen und Ankern der Schützin den Pfeil auf Höhe des Buttons. Dies wiederholt man ein paar Mal, um die Messung zu bestätigen. Am Ende kann man die Auszugslänge am Pfeil vom Nockboden bis zur Markierung ausmessen.\\

Zur Messung der Auszugslänge gibt es auch spezielle Messpfeile mit Markierungen zum direkten Ablesen. Den Messpfeil nicht verschiessen.\\

Die Auszugslänge spielt eine Rolle bei der Auswahl der richtigen Pfeile, vor allem wenn mit Klicker geschossen wird.



\subsection{Zuggewicht}

\subsubsection{Messen}

Das {\em Zuggewicht} (englisch {\em draw weight}) beschreibt, welche Kraft nötig ist, um den Bogen auszuziehen. Das Zuggewicht wird üblicherweise in Pfund (englisch {\em pound mass}) angegeben. Pfund wird auch auf zwei verschiedene Arten abgekürzt: 24 Pfund = 24\unit{lbs} = 24\unit{\#}. Ein Pfund entspricht etwa 0.453\unit{kg}.\\

Ein ersten Hinweis auf das Zuggewicht eines Bogens bietet ein Blick auf den Bogen oder die Wurfarme. Das Schild auf einem Recurve-Wurfarm zeigt zum Beispiel
\begin{quote}
\begin{tt}
25H 70-32 lbs\\
23H 68-34 lbs
\end{tt}
\end{quote}
Mit einem 25\unit{''} Mittelstück hat der Bogen eine Bogenlänge von 70\unit{''} und ein Zuggewicht von 32\unit{''}. Bei einem Mittelstück von 23\unit{''} ergibt sich eine Bogenlänge von 68\unit{''} und ein Zuggewicht von 34\unit{''}.\\

Das Zuggewicht hängt natürlich auch davon ab, wie gross die Auszugslänge der Schützin ist. Je weiter man zieht, desto mehr Kraft muss man aufwenden. Die Grössenangaben auf Bogen oder Wurfarmen beziehen sich immer auf eine Auszugslänge von 28\unit{''} (AMO draw length). Hat die Schützin eine grössere Auszugslänge, ist also das Zuggewicht, welches sie auf den Fingern hat grösser. Das kann pro Zoll zusätzlichem Auszug mehrere Pfund betragen!\\

Es gibt Messgeräte, welche das tatsächliche Zuggewicht der Schützin messen können. Sie werden unter dem Pfeil\footnote{Der Pfeil verhindert Schäden durch versehentliche Leerschüsse.} in der Sehne eingehängt. Die Schützin zieht den Bogen soweit aus wie sonst bei einem Schuss (zum Beispiel bis zum \Q{klick} des Klickers) und setzt wieder ab. Das Messgerät zeigt dann die maximale Kraft, also das Zuggewicht.

\subsubsection{Einstellen}

Das gewünschte Zuggewicht muss schon beim Bogenkauf berücksichtigt werden und kann nicht in grossem Umfang nachträglich verändert werden. Bei technischen Recurve-Bögen können die Wurfarme getauscht werden, um das Zuggewicht zu ändern.\\

In gewissem Umfang kann das Zuggewicht durch {\bf gleichmässiges} Anziehen der Tillerschrauben erhöht werden. Dies aber nur in beschränktem Umfang. Ein Hersteller gibt zum Beispiel einen Verstellbereich von 5\unit{\%} an, das macht bei 30\unit{lbs} gerade 1.5\unit{lbs} aus. Nach Verstellen der Tillerschrauben sollte man Standhöhe und Tiller kontrollieren!

\subsubsection{Sollwert}

Der Sollwert wird durch die körperlichen Fähigkeiten der Schützin und der Art des Schiessens (Indoor, Outdoor, Jagd) bestimmt. Für erwachsene Einsteigerinnen liegt das empfohlene Zuggewicht für Recurve-Bögen bei etwa 22-28\unit{lbs}, bei Jugendlichen und Kindern entsprechend tiefer. Trainierte Olympic-Recurve Athleten, welche zusätzlich Krafttraining betreiben, schiessen Bögen im Bereich von etwa 35-45\unit{lbs}.\\

Ein Test, den man als Recurve-Schützin machen kann ist dieser: Die Schützin hält den Bogen beim Schiessen 30\unit{s} lang im Vollauszug bevor sie den Schuss löst. Zeigt sich dabei grosses Zittern oder muss der Bogen gar abgesetzt werden, ist das Zuggewicht vermutlich zu hoch.\\

Möchte man sein Zuggewicht erhöhen, zum Beispiel durch Wechsel der Wurfarme, sollte man dies schrittweise tun. Wurfarme werden mit Zuggewichten in Schritten von 2\unit{lbs} verkauft, eine Erhöhung von 4\unit{lbs} pro Schritt tönt nicht nach viel ist aber genug. Spezifisches Krafttraining kann bei Erhöhung des Zuggewichts helfen.

\subsubsection{Auswirkungen}

Ein hohes Zuggewicht gerade bei Einsteigerinnen führt zu schlechter Form. Abläufe werden nicht sauber ausgeführt, wenn der Bogen bei dem Zuggewicht nicht sauber beherrscht wird. Damit prägen sich falsche Abläufe ein, die später schwer zu korrigieren sind. Bei schlechter Beherrschung des Zuggewichts sinkt die Genauigkeit der Schützin und auch das Verletzungsrisiko steigt.\\

Ein höheres Zuggewicht führt zu einer höheren Pfeilgeschwindigkeit, was mit einigen Vorteilen verbunden ist.



\subsection{Pfeilgeschwindigkeit}

\subsubsection{Messen}

Die {\em Pfeilgeschwindigkeit} (englisch {\em arrow speed}) wird in Fuss pro Sekunde (\unit{fps}) gemessen. Gemessen wird diese mit einem elektronischen Messgerät durch dass man den Pfeil schiesst. Eine Anzeige zeigt den Wert an.

\subsubsection{Einstellen}

Die Pfeilgeschwindigkeit hängt massgeblich vom Zuggewicht des Bogens sowie vom Gewicht des Pfeils ab.

\subsubsection{Auswirkungen}

Eine höhere Pfeilgeschwindigkeit führt zu einer kürzeren Flugzeit und einer flacheren Flugbahn. Beides sorgt dafür, dass der Pfeil weniger windanfällig ist, was gerade draussen und auf grössere Distanzen wichtig ist. Hinzu kommt, dass bei flacherer Flugbahn die Visiereinstellungen bei unterschiedlichen Entfernungen weniger weit auseinander liegen. Das führt dazu, dass der Fehlereinfluss bei falscher Schätzung der Entfernung zum Ziel bei höherer Pfeilgeschwindigkeit geringer ist, was gerade im 3D-Schiessen interessant ist.\\

Physikalisch gesehen hat ein Pfeil mit höherer Geschwindigkeit eine höhere Durchschlagskraft. Wird der Bogen zur Jagd eingesetzt, ist dies relevant und oft in Gesetzen umgesetzt (Mindest-Zuggewicht oder Mindest-Geschwindigkeit für Jagd-Bögen).



\subsection{Sehne}

\subsubsection{Länge}

Die korrekte Länge der Sehne lässt sich oft im Handbuch von Bogen, Mittelstück oder Wurfarmen nachschlagen. Ein Richtwert ist eine Länge, die runde 3.0-3.5\unit{''} kürzer ist als die Bogenlänge (siehe \tabref{string_length}).\\

\floattabstart{htb}{cc}
Bogenlänge & Sehnenlänge\\
\hline
62\unit{''} & 148.6 - 149.9cm\\
64\unit{''} & 153.7 - 154.9cm\\
66\unit{''} & 158.8 - 160.0cm\\
68\unit{''} & 163.8 - 165.1cm\\
70\unit{''} & 168.9 - 170.2cm\\
72\unit{''} & 174.0 - 175.3cm\\
\hline
\floattabend{string_length}{Sehnenlänge}

Die Länge lässt sich durch Eindrehen verkürzen, um die korrekte Standhöhe einzustellen. Verlängern lässt sie sich nicht. Beim Messen der Länge einer existierenden Sehne, muss man diese ausdrehen.

\subsubsection{Material}

Eine gute Bogensehne besitzt eine möglichst geringe Dehnung, damit die gesamte Energie des Auszugs in Geschwindigkeit des Pfeils umgesetzt wird. Je geringer Dehnung allerdings, desto grösser die Belastung des Bogens beim Schuss. Dies betrifft vor allem die Spitzen der Wurfarme. Die ersten Sehnen mit geringer Dehnung wurden als {\em FastFlight} Sehnen verkauft. Entsprechend ist eine wichtige Eigenschaft eines Bogens, ob er {\em FastFlight-tauglich} ist. Moderne Recurve-Bögen sind alle FastFlight-tauglich, es sind eher ältere Bögen oder traditionelle Recurve aus Holz, wo die Frage nach FastFlight-Tauglichkeit wichtig ist, falls eine performante Sehne gewünscht ist.\\

Für Sehnen gibt es eine Vielzahl von Materialien, einige sind in \figref{string_materials} aufgeschlüsselt nach Grundmaterial, Handelsnamen (für Garn und Sehnenmaterial) und das Anwendungsgebiet (von unten nach oben). Von links nach rechts ist die Abbildung nach Dehnbarkeit des Materials sortiert. Neben Naturstoffen (e.g. Flachs) ist {\tt Dacron} das einzige Material für nicht FastFlight-taugliche Bögen. Die meisten technischen Recurve-Bögen verwenden {\tt BCY 652}, {\tt BCY 8125} oder {\tt BCY X-99}. Materialien mit höherem {\em Vectran}-Anteil werden eher im Compound-Bereich eingesetzt.

\floatfig{drawings/string_materials.pdf}{htb}{0.8}{string_materials}{Sehnenmaterialien}

\subsubsection{Anzahl Stränge}

Sehnen werden aus mehreren Strängen eines Garns hergestellt. Die Anzahl der Stränge des Garns richtet sich nach dem Sehnenmaterial und dem Zuggewicht des Bogens. Je höher das Zuggewicht, desto mehr Stränge braucht die Sehne. Je stabiler das Sehnenmaterial, desto weniger. Für das Sehnenmaterial {\tt BCY 652 Spectra} finden sich in \tabref{number_of_strands} ein paar Richtwerte.\\

\floattabstart{htb}{cc}
Zuggewicht & Anzahl Stränge\\
\hline
20 - 29\unit{lbs} & 14\\
30 - 37\unit{lbs} & 16\\
38 - 42\unit{lbs} & 18\\
43 - 47\unit{lbs} & 20\\
\hline
\floattabend{number_of_strands}{Anzahl Stränge in Sehne}

Je mehr Stränge eine Sehne hat, desto schwerer ist sie und umso mehr Luftwiderstand hat sie. Daher resultieren mehr Stränge in einer geringeren Pfeilgeschwindigkeit aber auch in einem fehlerverzeihenderem System.\\

Je nach Anzahl der Stränge ist die Sehne dicker oder dünner. Zusammen mit der Mittelwicklung muss sie in der Dicke zu den Pfeilnocken passen.



\wrapfig{drawings/centershot.pdf}{l}{0.1}{centershot}{Centershot}

\subsection{Einstellung des Buttons}

Der Button hat zwei grundsätzliche Einstellungen: Wie weit der Knopf, an dem der Pfeil anliegt herausschaut (Centershot) und wie schwer er sich eindrücken lässt (Federhärte).

\subsubsection{Centershot}

Wurden die Wurfarme korrekt ausgerichtet, so dass die Sehne sauber über die Mitte der Wurfarme und dem Mittelstück verläuft, kann man einstellen, wie weit der Stempel des Buttons, an dem der Pfeil anliegt, herausschaut. Im besten Fall spannt man den Bogen aufrecht irgendwo ein, so dass man bequem aus der richtigen Blickrichtung auf den Bogen schauen kann. Alternativ hält man den Bogen am langen Arm von sich weg. Man legt einen Pfeil auf und nockt ihn ein. Nun richtet man den Bogen/Kopf so aus, dass die Sehne mittig über beide Wurfarme und das Mittelstück verläuft, wie in \figref{centershot} zu sehen. Der Button muss nun so eingestellt werden, dass der Pfeil leicht links von der Sehne zu sehen ist, etwa einen halben Pfeildurchmesser.\\

Die Abbildung und das beschriebene Vorgehen bezieht sich auf Rechtshand-Bögen. Für Linkshand-Bogen muss der Pfeil leicht rechts der Sehne zu sehen sein.


\subsubsection{Federhärte}

Die Federhärte des Buttons einzustellen ist komplexer. Die Hersteller liefern meist mehrere Federn (hart, mittel, weich) mit und dann lässt sich die Härte durch Drehung fein einstellen. Meist startet man mit einer mittleren Härte und findet durch Bareshaft-Tuning die korrekte Federhärte.

% Waage zum Voreinstellen
% Kopieren der Einstellungen eines Buttons auf einen anderen (Kaminski)



\section{Pfeile}
\subsection{Spine}
\subsection{Pfeillänge}
\subsection{Nock-Grösse}
\subsection{FOC}



\newpage
\pagenumbering{gobble}
\section*{Bogenpass}\vspace{1ex}

{\bf\large Schütze/Schützin}\\[2ex]
Name \hrulefill\hrulefill\ Datum \hrulefill\\[3ex]
Rechtshand\ $\square$ \hspace{4ex} Linkshand\ $\square$\\[3ex]
Auszugslänge (Zoll) \hrulefill\ Zuggewicht real (lbs) \hrulefill\\[2ex]

{\bf\large Mittelstück}\\[2ex]
Hersteller/Typ \hrulefill\\[3ex]
Länge (Zoll) \hrulefill\ Gesamtlänge (Zoll) \hrulefill\\[2ex]

{\bf\large Wurfarme}\\[2ex]
Hersteller/Typ \hrulefill\\[3ex]
Länge (Zoll) \hrulefill\ Zuggewicht (lbs) \hrulefill\\[2ex]

{\bf\large Sehne}\\[2ex]
FastFlight\ $\square$ \hspace{4ex} Länge (cm) \hrulefill\ Stränge \hrulefill\\[2ex]

{\bf\large Pfeile}\\[2ex]
Hersteller/Typ \hrulefill\\[3ex]
Spine \hrulefill\ Nockbodenlänge (cm)\ \hrulefill\\[2ex]

{\bf\large Einstellungen}\\[2ex]
Standhöhe (mm) \hrulefill\ Standhöhe Sollwert (mm) \hrulefill\\[3ex]
Nockpunkthöhe (mm)\hrulefill\\[3ex]
Tiller oben (mm) \hrulefill\ Tiller unten (mm) \hrulefill\ Tiller (mm) \hrulefill

\end{document}
