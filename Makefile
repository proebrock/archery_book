MAIN = book
SUBDIRS = drawings

.PHONY: all
all: $(MAIN).pdf

$(MAIN).pdf: $(MAIN).tex
	for dir in $(SUBDIRS); do \
		$(MAKE) -C $$dir; \
	done
	pdflatex -shell-escape $(MAIN).tex
	pdflatex -shell-escape $(MAIN).tex
#	biber $(MAIN).bcf
#	pdflatex -shell-escape $(MAIN).tex
#	pdflatex -shell-escape $(MAIN).tex

.PHONY: clean
clean:
	rm -rf _minted-$(MAIN) *.aux *.bbl *.bcf *.blg *.log *.nav *.out *.run.xml *.snm *.toc *vrb $(MAIN).pdf
	for dir in $(SUBDIRS); do \
		$(MAKE) -C $$dir clean; \
	done

